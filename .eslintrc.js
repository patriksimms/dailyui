module.exports = {
  root: true,
  // env: {
  //   node: true,
  //   es6: true,
  // },
  // plugins: [
  //   '@typescript-eslint',
  // ],
  // parserOptions: {
  //   parser: '@babel/eslint-parser'
  // },
  'parser': 'vue-eslint-parser',
  extends: [
    'plugin:vue/vue3-strongly-recommended',
    'plugin:@typescript-eslint/recommended',
    'eslint:recommended',
    // '@vue/standard'
  ],
  parserOptions: {
    'parser': '@typescript-eslint/parser',
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    yoda: 'off',
    'padded-blocks': 'off',
    'vue/multi-word-component-names': 'off'
  }
}
