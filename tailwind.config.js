module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}', '../src/router/index.ts'],
  mode: 'jit',
  theme: {
    fontFamily: {
      sans: ['Lato', 'sans-serif'],
      bold: ['Lato Bold 700'],
      credc: ['CREDC', 'serif']
    },
    colors: {
      purple: {
        '050': '#EAE2F8',
        100: '#CFBCF2',
        200: '#A081D9',
        300: '#8662C7',
        400: '#724BB7',
        500: '#653CAD',
        600: '#51279B',
        700: '#421987',
        800: '#34126F',
        900: '#240754'
      },
      red: {
        '050': '#FFE3E3',
        100: '#FFBDBD',
        200: '#FF9B9B',
        300: '#F86A6A',
        400: '#EF4E4E',
        500: '#E12D39',
        600: '#CF1124',
        700: '#AB091E',
        800: '#8A041A',
        900: '#610316'
      },
      blue: {
        '050': '#f5f7fa',
        100: '#D9E2EC',
        200: '#BCCCDC',
        300: '#9FB3C8',
        400: '#829AB1',
        500: '#627D98',
        600: '#486581',
        700: '#334E68',
        800: '#243B53',
        900: '#102A43'
      },
      teal: {
        '050': '#F0FCF9',
        100: '#C6F7E9',
        200: '#8EEDD1',
        300: '#5FE3C0',
        400: '#2DCCA7',
        500: '#17B897',
        600: '#079A82',
        700: '#048271',
        800: '#016457',
        900: '#004440'
      },
      gray: {
        50: '#fafafa',
        100: '#f4f4f5',
        200: '#e4e4e7',
        300: '#d4d4d8',
        400: '#a1a1aa',
        500: '#71717a',
        600: '#52525b',
        700: '#3f3f46',
        800: '#27272a',
        900: '#18181b',
      },
      yellow: {
        '050': '#FFFBEA',
        100: '#FFF3C4',
        200: '#FCE588',
        300: '#FADB5F',
        400: '#F7C948',
        500: '#F0B429',
        600: '#DE911D',
        700: '#CB6E17',
        800: '#B44D12',
        900: '#8D2B0B'
      },
      white: '#FFFFFF'
    },
    animation: {
      'bounce-slow': 'bounce 1.5s infinite'
    },
    extend: {
      inset: {
        '1/6': '16.66666%'
      },
      transitionProperty: {
        width: 'width'
      }
    }
  },
  variants: {},
  // plugins: [
  //   require('@tailwindcss/forms')
  // ]
}
