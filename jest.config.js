module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  globals: {
    'vue-jest': {
      babelConfig: true // using the babel.config.js of the project
    }
  },
  transform: {
    '^.+\\.vue$': 'vue3-jest', // using the vue-jest dependency to transpire Vue SFC to commonjs
    '^.+\\js$': 'babel-jest'  // using babel-jest to compile js files to commonjs via babel
  },
  moduleFileExtensions: ['vue', 'js', 'json', 'ts']
  //...
}
