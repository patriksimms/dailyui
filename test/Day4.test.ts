import Day4 from '../src/views/Day4.vue'
import { mount } from '@vue/test-utils'

describe('Day 4 Test suite', () => {

  it('number correctly set', () => {
    const c = mount(Day4, {})

    c.get('#btn-4').trigger('click')
    c.get('#btn-4').trigger('click')

    expect(c.vm.$data.currentNumber).toBe(44)
  })

  it('number and operator correctly set', () => {
    const c = mount(Day4, {})

    c.get('#btn-4').trigger('click')
    c.get('#btn-4').trigger('click')

    c.get('#btn-plus').trigger('click')

    expect(c.vm.$data.currentResult).toBe(44)
    expect(c.vm.$data.lastOperator).toBe('+')
  })

  it('number and operator correctly set and calculate', async () => {
    const c = mount(Day4, {})

    await c.get('#btn-4').trigger('click')

    await c.get('#btn-plus').trigger('click')
    await c.get('#btn-4').trigger('click')

    await c.get('#btn-calc').trigger('click')


    expect(c.vm.$data.currentResult).toBe(8)
  })

  it('correct calculation anf additional calculation after', async () => {
    const c = mount(Day4, {})

    await c.get('#btn-4').trigger('click')

    await c.get('#btn-plus').trigger('click')
    await c.get('#btn-4').trigger('click')

    await c.get('#btn-calc').trigger('click')

    await c.get('#btn-plus').trigger('click')
    await c.get('#btn-4').trigger('click')

    await c.get('#btn-calc').trigger('click')


    expect(c.vm.$data.currentResult).toBe(12)
  })

  it('correct decimal digits', async () => {
    const c = mount(Day4, {})

    await c.get('#btn-4').trigger('click')
    await c.get('#btn-colon').trigger('click')
    await c.get('#btn-4').trigger('click')
    await c.get('#btn-4').trigger('click')

    expect(c.vm.$data.currentNumber).toBe(4.44)
  })

  it('adding 2 decimal values', async () => {
    const c = mount(Day4, {})

    await c.get('#btn-4').trigger('click')
    await c.get('#btn-colon').trigger('click')
    await c.get('#btn-4').trigger('click')

    await c.get('#btn-plus').trigger('click')

    await c.get('#btn-4').trigger('click')
    await c.get('#btn-colon').trigger('click')
    await c.get('#btn-4').trigger('click')

    await c.get('#btn-calc').trigger('click')


    expect(c.vm.$data.currentResult).toBeCloseTo(8.8)
  })

  it('make number negative', async () => {
    const c = mount(Day4, {})

    await c.get('#btn-4').trigger('click')
    await c.get('#btn-negative').trigger('click')

    expect(c.vm.$data.currentNumber).toBe(-4)
  })
})
