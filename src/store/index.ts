import { createStore, useStore as baseUseStore, Store } from 'vuex'
import { InjectionKey } from 'vue'

export interface State {
  headerBaseColor: string
}

export const key: InjectionKey<Store<State>> = Symbol()

export const store = createStore<State>({
  state () {
    return {
      headerBaseColor: 'bg-purple-300'
    }
  },
  mutations : {
    setHeaderBaseColor(state, color) {
      state.headerBaseColor = color
    }
  },
  getters: {
    headerBaseColor (state) {
      return state.headerBaseColor
    }
  }
})

export function useStore () {
  return baseUseStore(key)
}
