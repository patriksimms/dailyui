import Home from '../views/Home.vue'
import Day2 from '../views/Day2.vue'
import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Day1 from '../views/Day1.vue'
import Day3 from '../views/Day3.vue'
import { store } from '../store'
import Day4 from '../views/Day4.vue'
import Day5 from '../views/Day5.vue'
import Day6 from '../views/Day6.vue'
import Day7 from '../views/Day7.vue'
import _404 from '../views/_404.vue'
import Day9 from '../views/Day9.vue'
import Day10 from '../views/Day10.vue'
import Day11 from '../views/Day11.vue'
import Day12 from '../views/Day12.vue'

export const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/day1',
    name: 'Day 1',
    component: Day1,
    meta: {
      date: '16 November 2020',
      description: 'A Sign Up Page for a Newsletter'
    }
  },
  {
    path: '/day2',
    name: 'Day 2',
    component: Day2,
    meta: {
      date: '7 September 2021',
      description: 'A Credit Card From'
    }
  },
  {
    path: '/day3',
    name: 'Day 3',
    component: Day3,
    meta: {
      date: '8 September 2021',
      description: 'A Product Landing Page',
      headerBaseColor: 'bg-teal-300'
    }
  },
  {
    path: '/day4',
    name: 'Day 4',
    component: Day4,
    meta: {
      date: '9 September 2021',
      description: 'A web based calculator'
    }
  },
  {
    path: '/day5',
    name: 'Day 5',
    component: Day5,
    meta: {
      date: '15 September 2021',
      description: 'An App Icon'
    }
  },
  {
    path: '/day6',
    name: 'Day 6',
    component: Day6,
    meta: {
      date: '17 September 2021',
      description: 'A user profile'
    }
  },
  {
    path: '/day7',
    name: 'Day 7',
    component: Day7,
    meta: {
      date: '8 Dezember 2021',
      description: 'Settings'
    }
  },
  {
    path: '/day8',
    name: 'Day 8',
    component: _404,
    meta: {
      date: '9 Dezember 2021',
      description: 'A 404 Page'
    }
  },
  {
    path: '/day9',
    name: 'Day 9',
    component: Day9,
    meta: {
      date: '12 Dezember 2021',
      description: 'A music Player',
      headerBaseColor: 'bg-teal-400'
    }
  },
  {
    path: '/day10',
    name: 'Day 10',
    component: Day10,
    meta: {
      date: '12 Dezember 2021',
      description: 'A social share button (and menu)',
    }
  },
  {
    path: '/day11',
    name: 'Day 11',
    component: Day11,
    meta: {
      date: '13 Dezember 2021',
      description: 'A flash message',
    }
  },
  {
    path: '/day12',
    name: 'Day 12',
    component: Day12,
    meta: {
      date: '15 Dezember 2021',
      description: 'A Single Item e-commerce Shop',
    }
  },
  {
    path: '/:pathMatch(.*)*',
    name: '404 - Not Found',
    component: _404
  }
]

export const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  window.document.title = to.name?.toString() ?? 'Patrik Simms Daily UI'

  if (to.meta.headerBaseColor) {
    store.commit('setHeaderBaseColor', to.meta.headerBaseColor)
  } else {
    store.commit('setHeaderBaseColor', 'bg-purple-300')
  }

  next()
})
