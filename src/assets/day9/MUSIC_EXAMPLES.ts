export interface Song {
  songName: string
  artist: string
  albumName: string
  albumCoverURL: string
  durationInMillis: number
}

// album cover art taken from apple music api
export const MUSIC_EXAMPLES: Song[] = [
  {
    songName: 'The Greatest',
    artist: 'Sia',
    albumCoverURL: 'https://is1-ssl.mzstatic.com/image/thumb/Music115/v4/8b/81/ba/8b81ba55-0a7f-a0e5-6593-d44f1565cdd1/886446097428.jpg/1000x1000bb.jpg',
    albumName: 'This Is Acting (Deluxe Version)',
    durationInMillis: 170533,
  },
  {
    songName: 'Running (feat. Meg Myers)',
    artist: 'Anyma',
    albumCoverURL: 'https://is3-ssl.mzstatic.com/image/thumb/Music116/v4/c8/84/40/c88440b8-3d67-d31c-da1d-415075232dac/8718522360350.png/1000x1000bb.jpg',
    albumName: 'Running - Single',
    durationInMillis: 180523,
  },
  {
    songName: 'Hurt (feat. Arctic Lake)',
    artist: 'Model Man',
    albumCoverURL: 'https://is4-ssl.mzstatic.com/image/thumb/Music115/v4/b6/7a/14/b67a1437-cdf8-6d9d-6929-7cd11f7eb8f4/cover.jpg/1000x1000bb.jpg',
    albumName: 'Model man',
    durationInMillis: 141235,
  },
  {
    songName: 'Fly Me To The Moon (Inspired By FINAL FANTASY XIV)',
    artist: 'Sia',
    albumCoverURL: 'https://is4-ssl.mzstatic.com/image/thumb/Music116/v4/36/05/47/360547a0-b20d-1587-5faf-1d43bb6f6052/075679762023.jpg/1000x1000bb.jpg',
    albumName: 'Fly Me To The Moon (Inspired By FINAL FANTASY XIV) - Single',
    durationInMillis: 141212,
  },
]
